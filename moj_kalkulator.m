% Matlab R2020b script
% name: moj_kalkulator.m
% author: maciej_pekala
% date: 2020-11-14
% version: v1.5


% zadanie 1
% deklaruje wartosc stalej Planca 
stala_planca = 6,62607004081*10^(-34); % J*s

result_1= stala_planca/(2*pi) % J*s



% zadanie 2
%deklaruje wartosc podstawy logarytmu naturalnego
e= exp(1);

result_2 = sin((pi/4)/e)


% zadanie 3
%deklaracja liczby w systemie dziesiatkowym
liczba_3 = hex2dec('0x0098d6');

result_3= liczba_3/(1.445*(10^23))

% zadanie 4

result_4= sqrt(e-pi)

% zadanie 5
% zamiana pi na string
pi_str=num2str(pi,15);

result_5 = pi_str(14)



% zadanie 6 
% data urodzin, 31 pazdzienik 1998, godz 6.20
urodziny= datetime(1998, 10, 31, 06, 20, 30);

% deklaracja obecnej daty 
dzis= datetime('now');

result_6= dzis-urodziny


% zadanie 7 
% deklaracja wartości promienia ziemi 
promien_ziemi= 6371000; % m

% wyliczenie potegi licznika
potega = (sqrt(7)/2)-log(R/(10^8));

% wyliczenie licznika
licznik= e^potega;

% deklaracja liczby w systemie dziesiatkowym
liczba_7= hex2dec('0xaaff');

result_7= atan(licznik/liczba_7)



% zadanie 8 
% deklaracja mola
mol= 6,02214076*(10^23);

% deklaracja umola
umol= mol*(10^(-6));

result_8= (9*umol)/4



% zadanie 9
% ilosc wszystkich wegli w alkoholu
i= (result_8*2)/9;

% liczba C13 
c13= i/101;

result_9= (c13/i)*1000 
